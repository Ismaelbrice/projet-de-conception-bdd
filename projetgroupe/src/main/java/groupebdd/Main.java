package groupebdd;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import groupebdd.entity.Commande;
import groupebdd.entity.Facturation;
import groupebdd.entity.Plats;
import groupebdd.entity.Reservation;
import groupebdd.repository.CommandeRepository;
import groupebdd.repository.FacturationRepository;
import groupebdd.repository.PlatsRepository;
import groupebdd.repository.ReservationRepository;

public class Main {
    
    public static void main(String[] args) {


////////////////  Afficher les reservations findAll ou findById ok ///////////

    ReservationRepository repoR = new ReservationRepository();
    /* List<Reservation> reserv = repoR.findAll();
        System.out.println(reserv.size());
    */

    /*   for(Reservation reservation : reserv){
            System.out.println(reservation.getId());
            System.out.println(reservation.getNombre_de_persone());
            System.out.println(reservation.getStartDate());
        }
    */
        /*Reservation reservation = repoR.findById(2);
        System.out.println(reservation.getNombre_de_persone());
        System.out.println(reservation.getStartDate());*/
        

        ///////delete reservation ok

        //System.out.println(repoR.delete(1));
        

        //////////Reservation persisit ok
     LocalDate locald = Date.valueOf("2024-10-10").toLocalDate();
/* 
    Reservation toPersist = new Reservation(locald, 15);
    System.out.println(repoR.persist(toPersist));
 */   
        ////////Reservation update 
  
        Reservation newReservation = repoR.findById(2);
        newReservation.setStartDate(locald);
        newReservation.setNombre_de_persone(100);
        newReservation.setId(1);
        System.out.println(repoR.update(newReservation));


        }
    }


