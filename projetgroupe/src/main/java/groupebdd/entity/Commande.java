package groupebdd.entity;


import java.sql.Date;
import java.time.LocalDate;

public class Commande {
    private Integer id;
    private LocalDate date;
    private Integer price;
    
    public Commande(Integer id, LocalDate date, Integer price) {
        this.id = id;
        this.date = date;
        this.price = price;
    }

    public Commande(LocalDate date, Integer price) {
        this.date = date;
        this.price = price;
    }

    public Commande() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

}
