package groupebdd.entity;

public class Facturation {

    private int id;
    private String moyen_de_payment;

    public Facturation(int id, String moyen_de_payment) {
        this.id = id;
        this.moyen_de_payment = moyen_de_payment;
    }
    
    public Facturation(String moyen_de_payment) {
        this.moyen_de_payment = moyen_de_payment;
    }
    
    public Facturation() {
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getMoyen_de_payment() {
        return moyen_de_payment;
    }
    public void setMoyen_de_payment(String moyen_de_payment) {
        this.moyen_de_payment = moyen_de_payment;
    }


}
