package groupebdd.entity;

public class Plats {
    private Integer id;
    private String name;
    private Integer price;
    private String description;
    private Integer quantiteDispo;

    
    public Plats() {
    }

    public Plats(String name, Integer price, String description, Integer quantiteDispo) {
        this.name = name;
        this.price = price;
        this.description = description;
        this.quantiteDispo = quantiteDispo;
    }

    public Plats(Integer id, String name, Integer price, String description, Integer quantiteDispo) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.description = description;
        this.quantiteDispo = quantiteDispo;
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Integer getPrice() {
        return price;
    }
    public void setPrice(Integer price) {
        this.price = price;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public Integer getQuantiteDispo() {
        return quantiteDispo;
    }
    public void setQuantiteDispo(Integer quantiteDispo) {
        this.quantiteDispo = quantiteDispo;
    }


}
