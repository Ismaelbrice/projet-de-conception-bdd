package groupebdd.entity;

import java.time.LocalDate;

public class Reservation {

    private int id;
    private LocalDate startDate ;
    private int Nombre_de_persone;
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public int getNombre_de_persone() {
        return Nombre_de_persone;
    }

    public void setNombre_de_persone(int nombre_de_persone) {
        Nombre_de_persone = nombre_de_persone;
    }

    public Reservation() {
    }

    public Reservation(LocalDate startDate, int nombre_de_persone) {
        this.startDate = startDate;
        Nombre_de_persone = nombre_de_persone;
    }

    public Reservation(int id, LocalDate startDate, int nombre_de_persone) {
        this.id = id;
        this.startDate = startDate;
        Nombre_de_persone = nombre_de_persone;
    }
    

    

}
