package groupebdd.entity;

public class Table {

    private Integer id;
    private Integer NbDePlace;

    
    public Table() {
    }
    public Table(Integer nbDePlace) {
        NbDePlace = nbDePlace;
    }
    public Table(Integer id, Integer nbDePlace) {
        this.id = id;
        NbDePlace = nbDePlace;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getNbDePlace() {
        return NbDePlace;
    }
    public void setNbDePlace(Integer nbDePlace) {
        NbDePlace = nbDePlace;
    }
    

}

