package groupebdd.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import groupebdd.entity.Commande;
import groupebdd.entity.Facturation;
import groupebdd.entity.Plats;

public class CommandeRepository {
    public List<Commande> findAll() {
        List<Commande> list = new ArrayList<>();
        
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM Commande");
            ResultSet result = stmt.executeQuery();

            
            while (result.next()) {
                Commande commande = new Commande(result.getInt("id"), result.getDate("date").toLocalDate(), result.getInt("price"));
                list.add(commande);
            }

        } catch (SQLException e) {
            System.out.println("Error From Repository");
            e.printStackTrace();
        }

        return list;
    }



    public boolean persist(Commande commande) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO Commandes (price, Date_de_commande) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setDate(2, Date.valueOf(commande.getDate()));
            stmt.setInt(1, commande.getPrice());
            
            if(stmt.executeUpdate() == 1) {
                ResultSet keys = stmt.getGeneratedKeys();
                keys.next();
                commande.setId(keys.getInt(1));
                return true;
            }

            
        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }
        return false;
    }

    public Commande findById(int id) {

        try (Connection connection = Database.connect()) {
            
            PreparedStatement stmt = connection.prepareStatement(
            "SELECT * ");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();

                if(result.next()){
                Commande commande = new Commande(
                result.getInt("id"),
                result.getDate("startDate").toLocalDate(),
                result.getInt("Nombre_de_persone"));
                return commande;
            }


        } catch (SQLException e) {
            System.out.println("Error From Repository");
            e.printStackTrace();
        }

        return null;
    }

    public Commande findByIdcCommande(int id_Commande) {

        try (Connection connection = Database.connect()) {
            
            PreparedStatement stmt = connection.prepareStatement(
            "SELECT * FROM Commandes  LEFT JOIN Plats_commandes ON Plats_commandes.id_commandes=Commandes.id WHERE id_commandes=?");
            stmt.setInt(1, id_Commande);
            ResultSet result = stmt.executeQuery();

            if(result.next()){
                Commande commande = new Commande(
                    result.getInt("id"),
                    result.getDate("startDate").toLocalDate(),
                result.getInt("Nombre_de_persone"));
                    return commande;
            }


        } catch (SQLException e) {
            System.out.println("Error From Repository");
            e.printStackTrace();
        }

        return null;
    }

    
    


}
