package groupebdd.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import groupebdd.entity.Facturation;

public class FacturationRepository {

    public List<Facturation> findAll(){
        List<Facturation> list = new ArrayList<>();

        try (Connection connection = Database.connect()){
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM facturation");
            ResultSet result = stmt.executeQuery();

            while(result.next()){
                Facturation facturation = new Facturation(
                    result.getInt("id"),
                    result.getString("moyen_de_payment"));
                list.add(facturation);
            }

        } catch (SQLException e) {
            System.out.println("Error from facturation");
            e.printStackTrace();
        }
        return list;
    }

    public Facturation findById(int id){

        try (Connection connection = Database.connect()){
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM person WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();

            if(result.next()){
                Facturation facturation = new Facturation(
                    result.getInt("id"),
                    result.getString("moyen_de_payment"));
                    return facturation;
            }

        } catch (SQLException e) {
            System.out.println("Error from facturation");
            e.printStackTrace();
        }
        
        return null;
    }




}
