package groupebdd.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import groupebdd.entity.Plats;
import groupebdd.entity.Reservation;

public class PlatsRepository {
    
    public List<Plats> findAll() {
        List<Plats> list = new ArrayList<>();
        
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM Plats");
            ResultSet result = stmt.executeQuery();
            
            while (result.next()) {
                Plats Plats = new Plats
                (result.getInt("id"),
                result.getString("name"),
                result.getInt("price"),
                result.getString("description"),
                result.getInt("quantiteDispo"));
            list.add(Plats);
            }

        } catch (SQLException e) {
            System.out.println("Error From Repository");
            e.printStackTrace();
        }
        return list;
    }

    public Reservation findById(int id){

        try (Connection connection = Database.connect()) {

            PreparedStatement stmt =  connection.prepareStatement("SELECT * FROM person WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();

            if (result.next()) {
                return new Reservation(
                        result.getInt("id"),
                        result.getDate("date").toLocalDate(),
                        result.getInt("nmbPerso"));
            }
        } catch (SQLException e) {
            System.out.println("Error From Reservation");
            e.printStackTrace();
        }
        return null;
    }


    public List<Plats> findcommandes() {
        List<Plats> list = new ArrayList<>();
        
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM Plats INNER JOIN Plats ON Commande.id_commandes=Plats.id");
            ResultSet result = stmt.executeQuery();

            
            while (result.next()) {
                Plats Plats = new Plats
                (result.getInt("id"),
                result.getString("name"),
                result.getInt("price"),
                result.getString("description"),
                result.getInt("quantiteDispo") );
                list.add(Plats);
            }

        } catch (SQLException e) {
            System.out.println("Error From Repository");
            e.printStackTrace();
        }

        return list;
    }


}

