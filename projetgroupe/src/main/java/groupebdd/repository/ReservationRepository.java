package groupebdd.repository;

import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import groupebdd.entity.Reservation;

/* 
import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
*/


public class ReservationRepository {

      /* @BeforeEach
    void setUp() {
        try {
            ScriptRunner runner = new ScriptRunner(Database.connect());
            runner.runScript(new InputStreamReader(getClass().getResourceAsStream("/database.sql")));
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }*/


    public List<Reservation>  findAll() {
        List<Reservation> list = new ArrayList<>();
        
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM Reservation");
            ResultSet result = stmt.executeQuery();
            
            while (result.next()) {
                Reservation reserv = new Reservation
                (result.getInt("id"),
                result.getDate("startDate").toLocalDate(),
                result.getInt("Nombre_de_persone"));
            list.add(reserv);
            }

        } catch (SQLException e) {
            System.out.println("Error From Reservation Repository");
            e.printStackTrace();
        }
        return list;
    }

    public Reservation findById(int id) {
        
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM Reservation WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            
            if (result.next()) {
                Reservation reserv = new Reservation
                (result.getInt("id"),
                result.getDate("startDate").toLocalDate(),
                result.getInt("Nombre_de_persone"));
                return reserv;
            }

        } catch (SQLException e) {
            System.out.println("Error From Repository");
            e.printStackTrace();
        }

        return null;
    }



    public boolean delete(int id) {

        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM Reservation  WHERE id=?");
            stmt.setInt(1, id);
            //Si on a bien une ligne qui a été affectée par la requête, on renvoie true
            if(stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            System.out.println("Error from repository delete");
            e.printStackTrace();
        }

        return false;
    }


    public boolean persist(Reservation reservation) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO Reservation (startDate,Nombre_de_persone) VALUES (?,?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setDate(1,  Date.valueOf(reservation.getStartDate()));
            stmt.setInt(2, reservation.getNombre_de_persone());
            
            if(stmt.executeUpdate() == 1) {
                ResultSet keys = stmt.getGeneratedKeys();
                keys.next();
                reservation.setId(keys.getInt(1));
                return true;
            }
            
        } catch (SQLException e) {
            System.out.println("Error from repository persist");
            e.printStackTrace();
        }
        return false;
    }


    public boolean update(Reservation reservation){

        try(Connection connection = Database.connect()) {
        PreparedStatement stmt = connection.prepareStatement("UPDATE Reservation SET startDate=?,Nombre_de_persone? WHERE id=?");
            stmt.setDate(1, Date.valueOf(reservation.getStartDate()));
            stmt.setInt(2, reservation.getNombre_de_persone());
            stmt.setInt(3, reservation.getId());

            return stmt.executeUpdate() == 1;
    
        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }

            return false;
        }
    


}
