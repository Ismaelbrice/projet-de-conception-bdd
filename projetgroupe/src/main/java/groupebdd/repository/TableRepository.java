package groupebdd.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import groupebdd.entity.Table;


    public class TableRepository {

    public Table findById(int id) {

        try (Connection connection = Database.connect()) {

            PreparedStatement stmt = 
            connection.prepareStatement("SELECT * FROM Tables WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();

            if (result.next()) {
                        Table maTable = new Table(

                            result.getInt("id"),
                            result.getInt("Nombre_de_place"));
                            return maTable;
                        
            }

        } catch (SQLException e) {
            System.out.println("Error From Repository");
            e.printStackTrace();
        }
        
        return null;
    }
}