-- Active: 1709545648155@@127.0.0.1@3306@java_jdbc

DROP Table IF EXISTS Tables_Reservation;
DROP Table IF EXISTS Plats_commandes;
DROP Table IF EXISTS Reservation;
DROP TABLE IF EXISTS Commandes ;
DROP TABLE IF EXISTS Tables ;
DROP TABLE IF EXISTS facturation ;
DROP TABLE IF EXISTS Plats ;


CREATE TABLE Plats (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(64) NOT NULL,
    price INT,
    description VARCHAR(64) NOT NULL,
    quantiteDispo INT
);

CREATE TABLE facturation(
    id INT PRIMARY KEY AUTO_INCREMENT ,
    moyen_de_paiment VARCHAR (150) NOT NULL
);

CREATE TABLE Tables (
    id INT PRIMARY KEY AUTO_INCREMENT,
    Nombre_de_place INT
);

CREATE TABLE Commandes (
    id INT PRIMARY KEY AUTO_INCREMENT,
    Date_de_commande DATE,
    price INT
);

CREATE TABLE Reservation(
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    startDate DATE,
    Nombre_de_persone INT
);

INSERT INTO `Plats` (name, price, description, `quantiteDispo`) VALUES
('Yassa', 15, 'oignons poulet citron', 10),
('Boeuf bourgignon',20, 'boeuf vin carrotte', 10),
('Sushi',30, 'riz poisson', 10),
('ramen', 25, 'legumes crevettes', 10),
('bouillabaisse', 30,'poisson legumes',35);

INSERT INTO facturation(moyen_de_paiment) VALUES
('Carte bancaire'),
('Espace'),
('Paypal'),
('Ticket restaurant');

INSERT INTO Tables (Nombre_de_place) VALUES
(5),
(5),
(8),
(8),
(10),
(10);

INSERT INTO Commandes (Date_de_commande, price, ) VALUES
("2005-01-04",150),
("2024-03-10", 50),
("2024-04-10", 50),
("2024-05-10", 50),
("2024-06-10", 50),
("2024-07-10", 50),
("2024-08-10", 50),
("2024-09-10", 50);

INSERT INTO Reservation (startDate, Nombre_de_persone) VALUES 
 ("2024-03-18",8),
 ("2020-05-7",5),
 ("2025-06-25",16);

CREATE TABLE Plats_commandes(
    id_Plats INT,
    id_commandes INT,
    PRIMARY KEY (id_Plats,id_Commandes),
    Foreign Key (id_Plats) REFERENCES Plats(id) ON DELETE CASCADE,
    Foreign Key (id_Commandes) REFERENCES Commandes(id) ON DELETE CASCADE
);

CREATE TABLE Tables_Reservation (
    id_Tables INT,
    id_Reservation INT,
    PRIMARY KEY (id_Tables,id_Reservation),
    Foreign Key (id_Tables) REFERENCES Tables(id) ON DELETE CASCADE,
    Foreign Key (id_Reservation) REFERENCES Commandes(id) ON DELETE CASCADE
);

INSERT INTO `Plats_commandes`(`id_Plats`, id_commandes) VALUES
(1,1),
(2,3),
(2,4),
(3,5),
(3,3),
(4,4),
(4,5),
(5,3),
(5,4),
(5,5);

INSERT INTO Tables_Reservation(id_Tables, id_Reservation) VALUES
(1,1),
(2,2),
(2,3),
(3,1),
(4,2);

SELECT * FROM `Reservation` ;