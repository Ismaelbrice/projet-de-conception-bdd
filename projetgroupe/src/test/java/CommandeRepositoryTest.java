import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import groupebdd.entity.Commande;
import groupebdd.repository.CommandeRepository;

public class CommandeRepositoryTest {
     @Test
    void persistSuccess() {
        CommandeRepository repo = new CommandeRepository();
        LocalDate date1 = LocalDate.of(2023, 5, 1);
        Commande commande = new Commande(1,date1,20);
        assertTrue(repo.persist(commande));
        assertEquals(20, commande.getId());
        assertEquals(date1, commande.getDate());
        assertEquals(20, commande.getPrice());
    }

}
