import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.Test;

import groupebdd.entity.Plats;
import groupebdd.repository.Database;
import groupebdd.repository.PlatsRepository;

public class PlatsRepositoryTest {
     void setUp() {
        try {
            ScriptRunner runner = new ScriptRunner(Database.connect());
            runner.runScript(new InputStreamReader(getClass().getResourceAsStream("/database.sql")));
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Test
    void findAllShouldReturnAListOfPlats() {
        PlatsRepository repo = new PlatsRepository();
        List<Plats> result = repo.findAll();

        
        assertEquals(5, result.size());
        assertEquals("Yassa", result.get(0).getName());
        assertEquals("oignons poulet citron", result.get(0).getDescription());
        assertEquals(10, result.get(0).getQuantiteDispo());
        assertEquals(1, result.get(0).getId());

        assertNotNull(result.get(0).getName());
        assertNotNull(result.get(0).getDescription());
        assertNotNull(result.get(0).getQuantiteDispo());
        assertNotNull(result.get(0).getId());
    }

}
