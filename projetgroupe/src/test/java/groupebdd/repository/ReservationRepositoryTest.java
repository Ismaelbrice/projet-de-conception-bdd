package groupebdd.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.Test;

import groupebdd.entity.Reservation;

public class ReservationRepositoryTest {
    @Test
    void findAllShouldReturnAListOfReservation() {
        ReservationRepository repo = new ReservationRepository();
        List<Reservation> result = repo.findAll();

        assertEquals(3, result.size());
        assertEquals("2024-03-18", result.get(0).getStartDate().toString());
        assertEquals(8, result.get(0).getNombre_de_persone());

        //assertNotNull(result.get(0).getStartDate().toString());
        //assertNotNull(result.get(0).getNombre_de_persone());
    }

    @Test
    void testFindByIdWithResultReservation() {
        ReservationRepository repo = new ReservationRepository();
        Reservation reservation  = repo.findById(1);

        assertEquals(1, reservation.getId());
        assertEquals("2024-03-18", reservation.getStartDate().toString());
        assertEquals(8, reservation.getNombre_de_persone());
    }

    @Test
    void testFindByIdNoResultReservation() {
        ReservationRepository repo = new ReservationRepository();
        Reservation reservation  = repo.findById(1000);

        assertNull(reservation);
    }

    //erreur
    @Test
    void deleteSuccess() {
        ReservationRepository repoD = new ReservationRepository();

        boolean result = repoD.delete(1);
        assertTrue(result);

        assertEquals(4, repoD.findAll().size());
    }

    @Test
    void persistSuccess() {

        ReservationRepository repo = new ReservationRepository();

        LocalDate locald = Date.valueOf("2024-10-10").toLocalDate();
        Reservation toPersist = new Reservation(locald, 15);
        assertTrue(repo.persist(toPersist));
        assertEquals(6, toPersist.getId());
    
    
    }


}
