package groupebdd.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import groupebdd.entity.Table;

public class TableRepositoryTest {
    
    
    @Test
    void findByIdWithResult() {
        TableRepository repo = new TableRepository();
        Table result = repo.findById(1);

        assertEquals(5, result.getNbDePlace());
        assertEquals(1, result.getId());
    }

    }

